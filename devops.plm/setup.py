from cdb.comparch.pkgtools import setup

setup(
    name="devops.plm",
    version="1.0.0",
    install_requires=['cs.platform', 'cs.mfa', 'cs.scm', 'cs.web', 'cs.base', 'cs.designsystem', 'cs.activitystream', 'cs.classification', 'cs.launchpad', 'cs.noteslink', 'cs.officelink', 'cs.taskboard', 'cs.powerreports', 'cs.taskmanager', 'cs.workflow', 'cs.actions', 'cs.metrics', 'cs.documents', 'cs.portfolios', 'cs.defects', 'cs.pcs', 'cs.vp', 'cs.bomcreator', 'cs.dsig', 'cs.ec', 'cs.projectcosts', 'cs.resources', 'cs.vp-pcs', 'cs.costing', 'cs.dsig-pdf', 'cs.innovation', 'cs.requirements', 'cs.workspaces', 'cs.cp', 'cs.pdx', 'cs.threed', 'cscdb.product'],
    docsets=[
        # Add a relative path for each documentation set in this package
        ],
    cdb_modules=[
        # List the package's modules in the correct (initialization) order as
        # computed by cdb.comparch topological sort. This list goes into
        # `cdb_modules.txt` in the EGG-INFO.
        "devops.plm"
        ],
    cdb_services=[
        # List the services of this packages by their class names. This list
        # goes into `cdb_services.txt` in EGG-INFO.
        ],
)
